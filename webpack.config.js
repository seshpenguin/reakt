const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const entryPoint = process.env.APP_ENTRY_POINT || './App.js';

module.exports = {
  entry: {
    main: entryPoint
  },
  output: {
    path: path.resolve("./dist"),
  },
  mode: "development",
  //devtool: "inline-cheap-module-source-map",
  module: {
    rules: [
      {
        test: /\.j?t?sx?$/,
        exclude: /node_modules\/(?!(plasma-reakt)\/).*/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ['@babel/preset-env', '@babel/preset-react', '@babel/preset-typescript']
          }
        }
      },
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.join(__dirname, "src", "index.html"),
      publicPath: "qrc:"
    }),
  ],
  resolve: {
    alias: {
      'react-native$': 'react-native-web'
    },
    extensions: ['.js', '.jsx', '.ts', '.tsx'],
  }
}
