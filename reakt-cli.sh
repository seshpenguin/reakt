#!/usr/bin/env bash
set -e
echo "Compiling Plasma Reakt Project..."
pwd
# from https://stackoverflow.com/questions/59895/how-do-i-get-the-directory-where-a-bash-script-is-located-from-within-the-script
SOURCE=${BASH_SOURCE[0]}
while [ -L "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR=$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )
  SOURCE=$(readlink "$SOURCE")
  [[ $SOURCE != /* ]] && SOURCE=$DIR/$SOURCE # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR=$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )
echo ${DIR}

BROWSERSLIST="chrome 87" ./node_modules/.bin/webpack build --config ${DIR}/webpack.config.js
echo "React Native JS components compiled!"

echo "Packaging with Plasma Reakt runtime..."
mkdir -pv ${DIR}/reakt-runtime/src/contents/web
cp ./dist/* ${DIR}/reakt-runtime/src/contents/web/
ls -l ${DIR}/reakt-runtime/src/contents/web/
pushd .
    cd ${DIR}/reakt-runtime
    cmake -B build/ . && cmake --build build/
popd
cp ${DIR}/reakt-runtime/build/bin/reakt dist/

echo "Done!"