import styled from "styled-components/native";


export default styled.TextInput.attrs({
  placeholderTextColor: "lightgrey",
})`
  height: 35px;
  margin: 12px;
  border: 1px solid lightgray;
  border-radius: 4px;
  background-color: white;
  padding: 5px
`;