import React, {useEffect} from "react";
import { AppRegistry } from "react-native";

interface ReaktAPI {
  appSheet: {
      open: () => void
      contentItem: {
          text: String
      }
      title: {
          text: String
      }
  }
  palette: any
  app: {
    showWindow: () => void
  }
};

export let reakt: ReaktAPI;

/** Establishes the QWebChannel connection, sets up the initial React component to render,
 * and shows the window when ready. This should only be called once. */
export function registerRootComponent(EntryPoint: React.ComponentType) {
  const ReaktApp: React.ComponentType = () =>{
    useEffect(() => {
      // Show window on first render
      console.log("Plasma Reakt app started 🚀")
      reakt.app.showWindow();
    }, []);
    return (
      <div>
        <EntryPoint />
      </div>
    );
  }

  // @ts-ignore
  new QWebChannel(qt.webChannelTransport, function(channel) {
    console.log("QWebChannel connected.");
    console.log(channel.objects);
    reakt = channel.objects;

    // @ts-ignore
    window.reaktWebChannel = channel;
    AppRegistry.registerComponent("App", () => ReaktApp);
    AppRegistry.runApplication("App", {
      rootTag: document.getElementById("root")
    });
  });
}

// todo make this a component
/** Access a basic text-only Kirigami AppSheet. */
export const appSheet = {
  open: () => reakt.appSheet.open(),
  setText: (text) => reakt.appSheet.contentItem.text = text,
  setTitle: (title) => reakt.appSheet.title = title,
}

/** Returns whether the app is running in the Plasma Reakt runtime environment. */
export const isReaktRuntime = () => reakt !== undefined;

export function usePalette() {
  const [palette, setPalette] = React.useState({
    base: reakt.palette.base,
    button: reakt.palette.button,
    buttonText: reakt.palette.buttonText,
    dark: reakt.palette.dark,
    highlight: reakt.palette.highlight,
    highliteText: reakt.palette.highliteText,
    light: reakt.palette.light,
    mid: reakt.palette.mid,
    midlight: reakt.palette.midlight,
    shadow: reakt.palette.shadow,
    text: reakt.palette.text,
    window: reakt.palette.window,
    windowText: reakt.palette.windowText,
  });
  useEffect(() => {
    const listener = () => {
      setPalette({
        base: reakt.palette.base,
        button: reakt.palette.button,
        buttonText: reakt.palette.buttonText,
        dark: reakt.palette.dark,
        highlight: reakt.palette.highlight,
        highliteText: reakt.palette.highliteText,
        light: reakt.palette.light,
        mid: reakt.palette.mid,
        midlight: reakt.palette.midlight,
        shadow: reakt.palette.shadow,
        text: reakt.palette.text,
        window: reakt.palette.window,
        windowText: reakt.palette.windowText,
      })
    };
    reakt.palette.paletteChanged.connect(listener);
    return () => reakt.palette.paletteChanged.disconnect(listener);
  } , []);
  return palette;
}


// Components
export { default as TextInput } from "./components/TextInput";
