/*
    SPDX-License-Identifier: GPL-2.0-or-later
    SPDX-FileCopyrightText: 2022 Seshan Ravikumar <seshan10@me.com>
*/

#include <QApplication>
#include <QQmlApplicationEngine>
#include <QUrl>
#include <QtQml>
#include <QtWebEngine/qtwebengineglobal.h>
#include <QtWebChannel/qqmlwebchannel.h>
#include <QPalette>

#include "about.h"
#include "app.h"
#include "version-reakt.h"
#include <KAboutData>
#include <KLocalizedContext>
#include <KLocalizedString>
#include <Kirigami/PlatformTheme>

#include "reaktconfig.h"


Q_DECL_EXPORT int main(int argc, char *argv[])
{
    QLoggingCategory::setFilterRules("js.info=true\n");
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication::setAttribute(Qt::AA_ShareOpenGLContexts);

    QtWebEngine::initialize();

    QApplication app(argc, argv);
    QCoreApplication::setOrganizationName(QStringLiteral("KDE"));
    QCoreApplication::setApplicationName(QStringLiteral("reakt"));

    KAboutData aboutData(
                         // The program name used internally.
                         QStringLiteral("reakt"),
                         // A displayable program name string.
                         i18nc("@title", "reakt"),
                         // The program version string.
                         QStringLiteral(REAKT_VERSION_STRING),
                         // Short description of what the app does.
                         i18n("Application Description"),
                         // The license this code is released under.
                         KAboutLicense::GPL,
                         // Copyright Statement.
                         i18n("(c) 2022"));
    aboutData.addAuthor(i18nc("@info:credit", "Seshan Ravikumar"),
                        i18nc("@info:credit", "Author Role"),
                        QStringLiteral("seshan10@me.com"),
                        QStringLiteral("https://yourwebsite.com"));
    KAboutData::setApplicationData(aboutData);

    QQmlApplicationEngine engine;

    auto config = reaktConfig::self();

    qmlRegisterSingletonInstance("org.kde.reakt", 1, 0, "Config", config);

    AboutType about;
    qmlRegisterSingletonInstance("org.kde.reakt", 1, 0, "AboutType", &about);

    // WebChannel bridges the Qt world with the JS world over Chromium IPC
    QQmlWebChannel *channel = new QQmlWebChannel();

    App application;
    qmlRegisterSingletonInstance("org.kde.reakt", 1, 0, "App", &application);
    channel->registerObject("app", &application);

    engine.rootContext()->setContextObject(new KLocalizedContext(&engine));
    engine.rootContext()->setContextProperty("channel", channel);
    engine.load(QUrl(QStringLiteral("qrc:///main.qml")));

    application.window = qobject_cast<QQuickWindow*>(engine.rootObjects()[0]);

    if (engine.rootObjects().isEmpty()) {
        return -1;
    }

    return app.exec();
}
