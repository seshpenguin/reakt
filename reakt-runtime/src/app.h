// SPDX-License-Identifier: GPL-2.0-or-later
// SPDX-FileCopyrightText: 2022 Seshan Ravikumar <seshan10@me.com>

#pragma once

#include <QObject>
#include <QQuickWindow>


class QQuickWindow;

class App : public QObject
{
    Q_OBJECT
private:

public:
    QQuickWindow *window;

    // Restore current window geometry
    Q_INVOKABLE void restoreWindowGeometry(QQuickWindow *window, const QString &group = QStringLiteral("main")) const;
    // Save current window geometry
    Q_INVOKABLE void saveWindowGeometry(QQuickWindow *window, const QString &group = QStringLiteral("main")) const;

    Q_INVOKABLE void showWindow();

    Q_INVOKABLE void showAppSheet(QString title, QString content);
};
