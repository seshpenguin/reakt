// SPDX-License-Identifier: GPL-2.0-or-later
// SPDX-FileCopyrightText: 2022 Seshan Ravikumar <seshan10@me.com>

import QtQuick 2.15
import QtQuick.Controls 2.15 as Controls
import QtQuick.Layouts 1.15
import org.kde.kirigami 2.19 as Kirigami
import org.kde.reakt 1.0
import QtWebEngine 1.5
import QtWebChannel 1.0

Kirigami.ApplicationWindow {
    id: root
    visible: false

    title: i18n("reakt")

    minimumWidth: Kirigami.Units.gridUnit * 20
    minimumHeight: Kirigami.Units.gridUnit * 20

    onClosing: App.saveWindowGeometry(root)

    onWidthChanged: saveWindowGeometryTimer.restart()
    onHeightChanged: saveWindowGeometryTimer.restart()
    onXChanged: saveWindowGeometryTimer.restart()
    onYChanged: saveWindowGeometryTimer.restart()

    Component.onCompleted: App.restoreWindowGeometry(root)


    // This timer allows to batch update the window size change to reduce
    // the io load and also work around the fact that x/y/width/height are
    // changed when loading the page and overwrite the saved geometry from
    // the previous session.
    Timer {
        id: saveWindowGeometryTimer
        interval: 1000
        onTriggered: App.saveWindowGeometry(root)
    }


    SystemPalette { id: palette; colorGroup: SystemPalette.Active }

    globalDrawer: Kirigami.GlobalDrawer {
        title: i18n("reakt")
        titleIcon: "applications-graphics"
        isMenu: !root.isMobile
        actions: [
            /*Kirigami.Action {
                text: i18n("Open DevTools")
                icon.name: "list-add"
                onTriggered: {
                    var component = Qt.createComponent("DevToolsView.qml")
                    var window  = component.createObject(root)
                    window.show()

                    web.devToolsView = devroot.devtoolsweb
                }
            },*/
            Kirigami.Action {
                text: i18n("About reakt")
                icon.name: "help-about"
                onTriggered: pageStack.layers.push('qrc:About.qml')
            },
            Kirigami.Action {
                text: i18n("Quit")
                icon.name: "application-exit"
                onTriggered: Qt.quit()
            }
        ]
    }

    contextDrawer: Kirigami.ContextDrawer {
        id: contextDrawer
    }

    pageStack.initialPage: page

    Kirigami.Page {
        id: page

        Layout.fillWidth: true

        title: web.title

        /*actions.main: Kirigami.Action {
            text: i18n("AAA")
            icon.name: "list-add"
            tooltip: i18n("Add one to the counter")
            onTriggered: {
                appSheet.open()
                root.height = root.height - 200
            }
        }*/

        WebEngineView {
            id: web
            anchors.fill: parent
            webChannel: channel
            backgroundColor: "transparent"
            url: 'qrc:index.html'
            Component.onCompleted: {
                console.log("Registering QObjects...")
                web.webChannel.registerObjects({
                    appSheet: appSheet,
                    palette: palette
                })
                console.log("WebEngine complete!")
            }

        }

        Kirigami.OverlaySheet {
            id: appSheet

            parent: applicationWindow().overlay

            Controls.Label {
                Layout.fillWidth: true
                wrapMode: Text.WordWrap
                text: "No Content Set"
            }
        }

    }

}
