import QtQuick 2.15
import QtQuick.Controls 2.15
import QtWebEngine 1.5

ApplicationWindow {
    id: devroot
    width: 1024; height: 768

    WebEngineView {
        id: devtoolsweb
        anchors.centerIn: parent
        width: parent.width
        height: parent.height
    }
}
