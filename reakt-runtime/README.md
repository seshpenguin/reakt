# Reakt Runtime
This is the Qt application that runs the web app. QtWebEngine (Chromium) is used to render the app, and QtWebChannel
faciliates communication between the web and Qt worlds (over Chromium IPC).
