# Plasma Reakt

Experiment: Create Plasma Mobile apps using React Native and other web technologies.

🚧 Under Construction! 🚧 

![Screenshot of a Plasma Reakt window](docs/reakt-window.png)

## Description
This is a project that aims to allow developers to build and/or port React Native applications to the Plasma Mobile platform.

This repository is an npm package which contains the "plasma-reakt" JS library, and the "reakt-runtime" Qt application.

The package is not published on npm yet, but you can use `npm pack` to build a local copy.

### plasma-reakt
The JS library part of this project does two main things: provide a set of KDE-themed React Native components (can be used anywhere), and provide a set of React hooks that interact with the native Qt and underlying Linux OS world (can only be used with reakt-runtime).

### reakt-runtime
Reakt Runtime is the Qt application that hosts the React Native Web application, using [QtWebEngine](https://wiki.qt.io/QtWebEngine) to render the UI. QtWebEngine uses Chromium behind the scenes, and integrates the layer rendering of Chromium directly into the OpenGL scene graph of Qt Quick.

Communication between the Qt and web world is done using [QtWebChannel](https://doc.qt.io/qt-6/qtwebchannel-index.html), which exposes QObjects into the React Native application (over Chromium IPC).

## Building an App

The most basic Plasma Reakt app contains a App.js file that looks like this:
```javascript
import React from "react";
import { View, Text } from "react-native";
import { registerRootComponent } from 'plasma-reakt';

function App() {
  return (
    <View>
      <Text>Hello Plasma Reakt!</Text>
    </View>
  );
}

registerRootComponent(App);
```

To build your application, add `"build": "reakt"` to the scripts section of your *package.json*. Then, run `npm run build`, which if successful, will give you a binary named `reakt` in the dist folder (the other files in dist, including main.js, are not needed and are built into the binary already).

## Running the Built-in Example App
If you would like to quickly experience Plasma Reakt, there is a small example app in this repository.

Just run `npm start` to build and run it.